﻿using Microsoft.Data.Sqlite;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ConsoleApp1
{
    class Program
    {
        const int port = 8888;
        static NetworkStream stream;
        static TcpClient client;

        static void Main(string[] args)
        {
            TcpListener server = null;
            try
            {
                IPAddress localAddr = IPAddress.Parse("127.0.0.1");
                server = new TcpListener(localAddr, port);
                // запуск слушателя
                server.Start();
                while (true)
                {
                    Console.WriteLine("Ожидание подключений... ");
                    client = server.AcceptTcpClient();
                    Console.WriteLine("Клиент подключился к серверу.");
                    stream = client.GetStream();

                    while (client.Connected)
                    {
                        byte[] cmd = new byte[256];
                        StringBuilder builder = new StringBuilder();
                        int bytes = 0;
                        do
                        {
                            bytes = stream.Read(cmd, 0, cmd.Length);
                            builder.Append(Encoding.UTF8.GetString(cmd, 0, bytes));
                        }
                        while (stream.DataAvailable);

                        string message = builder.ToString();
                        Console.WriteLine(message);
                        stream.Flush();
                        if (message.Contains("SELECT"))
                        {
                            if (message.Contains("Password"))
                            {
                                string rtrn = null;
                                SqliteConnection connect = new SqliteConnection("Data source = accounts.db");
                                connect.Open();
                                SqliteCommand command = new SqliteCommand(message, connect);
                                command.ExecuteNonQuery();
                                SqliteDataReader Reader = command.ExecuteReader();
                                while (Reader.Read())
                                {
                                    rtrn = Reader["Password"].ToString();
                                }
                                Reader.Close();
                                byte[] nazad = Encoding.UTF8.GetBytes(rtrn);
                                stream.Write(nazad, 0, nazad.Length);
                            }
                            if (message.Contains("Name"))
                            {
                                string namertrn = null;
                                object name = namertrn;
                                SqliteConnection connect = new SqliteConnection("Data source = accounts.db");
                                connect.Open();
                                SqliteCommand command = new SqliteCommand(message, connect);
                                command.ExecuteNonQuery();
                                SqliteDataReader Reader = command.ExecuteReader();
                                while (Reader.Read())
                                {
                                    namertrn = Reader["Name"].ToString();
                                }
                                Reader.Close();
                                Console.WriteLine(namertrn);
                                byte[] namenazad = Encoding.UTF8.GetBytes(namertrn);
                                stream.Write(namenazad, 0, namenazad.Length);
                            }
                            if (message.Contains("Surname"))
                            {
                                string surnamertrn = null;
                                SqliteConnection connect = new SqliteConnection("Data source = accounts.db");
                                connect.Open();
                                SqliteCommand command = new SqliteCommand(message, connect);
                                command.ExecuteNonQuery();
                                SqliteDataReader Reader = command.ExecuteReader();
                                while (Reader.Read())
                                {
                                    surnamertrn = Reader["Surname"].ToString();
                                }
                                Reader.Close();
                                Console.WriteLine(surnamertrn);
                                byte[] surnamenazad = Encoding.UTF8.GetBytes(surnamertrn);
                                stream.Write(surnamenazad, 0, surnamenazad.Length);
                            }
                            if (message.Contains("Image"))
                            {
                                SqliteConnection connect = new SqliteConnection("Data source = accounts.db");
                                connect.Open();
                                SqliteCommand command = new SqliteCommand(message, connect);
                                command.ExecuteNonQuery();
                                SqliteDataReader Reader = command.ExecuteReader();
                                while (Reader.Read())
                                {
                                    string foto = (string)Reader["Image"];
                                    byte[] image = Convert.FromBase64String(foto);
                                    if (stream.CanWrite)
                                    {
                                        stream.Write(image, 0, image.Length);
                                        stream.Flush();
                                    }
                                }
                            }
                            if (message.Contains("*"))
                            {
                                SqliteConnection connect = new SqliteConnection("Data source = accounts.db");
                                connect.Open();
                                SqliteCommand command = new SqliteCommand(message, connect);
                                SqliteDataReader reader = command.ExecuteReader();

                                List<string[]> data = new List<string[]>();
                                while (reader.Read())
                                {
                                    var buf = new string[reader.FieldCount];

                                    for (var i = 0; i < buf.Length; i++)
                                        buf[i] = reader.GetString(i);

                                    data.Add(buf);
                                }
                                string serialized = JsonConvert.SerializeObject(data);
                                byte[] tabliza = Encoding.UTF8.GetBytes(serialized);
                                if (stream.CanWrite)
                                {
                                    stream.Write(tabliza, 0, tabliza.Length);
                                    stream.Flush();
                                }

                            }
                            if (message.Contains("CashAmount"))
                            {
                                string cash = null;
                                SqliteConnection connect = new SqliteConnection("Data source = accounts.db");
                                connect.Open();
                                SqliteCommand command = new SqliteCommand(message, connect);
                                command.ExecuteNonQuery();
                                SqliteDataReader Reader = command.ExecuteReader();

                                while (Reader.Read())
                                {
                                    cash = Reader["CashAmount"].ToString();
                                }
                                Reader.Close();
                                Console.WriteLine();
                                byte[] cashnazad = Encoding.UTF8.GetBytes(cash);
                                stream.Write(cashnazad, 0, cashnazad.Length);
                            }
                        }
                        if (message.Contains("INSERT"))
                        {
                            SqliteConnection connect = new SqliteConnection("Data source = accounts.db");
                            connect.Open();
                            SqliteCommand command = new SqliteCommand(message, connect);
                            command.ExecuteNonQuery();
                        }
                        if (message.Contains("UPDATE"))
                        {
                            SqliteConnection connect = new SqliteConnection("Data source = accounts.db");
                            connect.Open();
                            SqliteCommand command = new SqliteCommand(message, connect);
                            command.ExecuteNonQuery();
                        }
                    }
                    stream.Close();
                    client.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}

