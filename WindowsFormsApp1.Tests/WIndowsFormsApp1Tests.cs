﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Sockets;

namespace WindowsFormsApp1.Tests
{
    [TestClass]
    public class WIndowsFormsApp1Tests
    {
        static TcpClient Client = new TcpClient();
        [TestMethod]
        public void peremeshenie()
        {
            //arrange
            FormUser a = new FormUser(ref Client);
            FormPayment b = new FormPayment(ref Client);
            FormHistory c = new FormHistory(ref Client);
            //act
            int actual;
            int expected = 1;
            a.Show();
            a.MakePayment.PerformClick();
            b.Show();
            b.button1.PerformClick();
            c.Show();
            c.UserOpen.PerformClick();
            a.Show();
            actual = c.k;
            //assert
            Assert.AreEqual(expected, actual);
        }
    }
}
