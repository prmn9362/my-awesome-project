using System;
using System.Data.SQLite;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class FormPayment : Form
    {
        TcpClient Client;
        static NetworkStream stream;
        public FormPayment(ref TcpClient client)
        {
            InitializeComponent();
            Client = client;
        }

        private void User_Click(object sender, EventArgs e)
        {
            Hide();
            new FormUser(ref Client).Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Hide();
            new FormHistory(ref Client).Show();
        }

        private void CheckCash_Click(object sender, EventArgs e)
        {
            if (MinusCash.Text == "")
            {
                MessageBox.Show("Вы не ввели логин!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                stream = Client.GetStream();
                string checkcash = $"SELECT CashAmount FROM users WHERE Login = '{MinusCash.Text}'";
                byte[] check = Encoding.UTF8.GetBytes(checkcash);
                stream.Write(check, 0, check.Length);

                byte[] GetCash = new byte[64];
                StringBuilder builder1 = new StringBuilder();
                do
                {
                    int byte1 = stream.Read(GetCash, 0, GetCash.Length);
                    builder1.Append(Encoding.UTF8.GetString(GetCash, 0, byte1));
                }
                while (stream.DataAvailable);
                CheckCashField.Text = builder1.ToString();
                stream.Flush();
            }
        }

        private void TransferCash_Click(object sender, EventArgs e)
        {
            try
            {
                stream = Client.GetStream();
                DateTime time = DateTime.Now;
                string dengi = textBox2.Text;
                int cash = Convert.ToInt32(dengi);
                string update1 = $"UPDATE users SET CashAmount = CashAmount - '{cash}' WHERE Login = '{MinusCash.Text}'";
                string update2 = $"UPDATE users SET CashAmount = CashAmount + '{cash}' WHERE Login = '{PlusCash.Text}'";
                string insert = $"INSERT INTO PaymentHistory (Otpravitel, Poluchatel, KolvoDeneg, Vremya) VALUES('{MinusCash.Text}','{PlusCash.Text}','{textBox2.Text}','{time}')";
                byte[] updt1 = Encoding.UTF8.GetBytes(update1);
                stream.Write(updt1, 0, updt1.Length);
                stream.Flush();
                Thread.Sleep(1000);
                byte[] updt2 = Encoding.UTF8.GetBytes(update2);
                stream.Write(updt2, 0, updt2.Length);
                stream.Flush();
                Thread.Sleep(1000);
                byte[] nsrt = Encoding.UTF8.GetBytes(insert);
                stream.Write(nsrt, 0, nsrt.Length);
                stream.Flush();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                MessageBox.Show("Операция выполнена успешно!");
            }
                
        }
        private void FormPayment_FormClosing(object sender, FormClosingEventArgs e)
        {
            Hide();
            new LoginForm(ref Client).Show();
        }
    }
}
