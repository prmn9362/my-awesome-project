using System;
using System.Net.Sockets;
using System.Text;
using System.Data.SQLite;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class RegisterForm : Form
    {
        TcpClient Client;
        public RegisterForm(ref TcpClient client)
        {
            InitializeComponent();
            Client = client;
        }
        private void RegisterButton_Click(object sender, EventArgs e)
        {
            if ((UserPassCheckField.Text != null) || (UserPassField.Text != null) || (UserLoginField.Text != null) || (CardNumber.Text != null) || (CashAmount.Text != null))
            {
                if (UserPassCheckField.Text == UserPassField.Text)
                    try
                    {
                        int Cash = Convert.ToInt32(CashAmount.Text);
                        NetworkStream stream = Client.GetStream();
                        string Message = $"INSERT INTO users (Login, Password, CardNumber, CashAmount) VALUES ('{UserLoginField.Text}','{UserPassField.Text}', '{CardNumber.Text}','{Cash}')";
                        byte[] command = Encoding.UTF8.GetBytes(Message);
                        stream.Write(command, 0, command.Length);
                        stream.Flush();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    finally
                    {
                        MessageBox.Show($"Пользователь {UserLoginField.Text} успешно добавлен!");
                        Hide();
                        new LoginForm(ref Client).Show();
                    }
                else
                {
                    MessageBox.Show("Пароли не совпадают!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Необходимо заполнить все поля!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void RegisterForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Hide();
            new LoginForm(ref Client).Show();
        }
    }
}





