using Newtonsoft.Json;
using System;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp1
{

    public partial class FormHistory : Form
    {
        TcpClient Client;
        public int k = 0;

        public FormHistory(ref TcpClient client)
        {
            InitializeComponent();
            Client = client;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Hide();
            new FormPayment(ref Client).Show();
        }

        private void UserOpen_Click(object sender, EventArgs e)
        {
            k = 1;
            Hide();
            new FormUser(ref Client).Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                dataGridView1.Rows.Clear();
                NetworkStream stream = Client.GetStream();
                string gettable = $"SELECT * FROM PaymentHistory WHERE Otpravitel = '{LoginField.Text}' OR Poluchatel = '{LoginField.Text}'";
                byte[] table = Encoding.UTF8.GetBytes(gettable);
                stream.Write(table, 0, table.Length);

                byte[] GetTable = new byte[256];


                StringBuilder builder1 = new StringBuilder();
                do
                {
                    int byte1 = stream.Read(GetTable, 0, GetTable.Length);
                    builder1.Append(Encoding.UTF8.GetString(GetTable, 0, byte1));
                }
                while (stream.DataAvailable);
                string Table = builder1.ToString();
                var data = JsonConvert.DeserializeObject<string[][]>(Table);
                foreach (string[] s in data)
                    dataGridView1.Rows.Add(s);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
            if (LoginField.Text == "")
            {
                MessageBox.Show("Вы не ввели логин!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FormHistory_FormClosing(object sender, FormClosingEventArgs e)
        {
            Hide();
            new LoginForm(ref Client).Show();
        }
    }
}
