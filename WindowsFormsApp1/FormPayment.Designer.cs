
namespace WindowsFormsApp1
{
    partial class FormPayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.User = new System.Windows.Forms.Button();
            this.CheckCash = new System.Windows.Forms.Button();
            this.CheckCashField = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.MinusCash = new System.Windows.Forms.TextBox();
            this.TransferCash = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.PlusCash = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.OrangeRed;
            this.button1.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(436, 374);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(232, 39);
            this.button1.TabIndex = 9;
            this.button1.Text = "Посмотреть историю";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // User
            // 
            this.User.BackColor = System.Drawing.Color.OrangeRed;
            this.User.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.User.Location = new System.Drawing.Point(102, 374);
            this.User.Name = "User";
            this.User.Size = new System.Drawing.Size(210, 39);
            this.User.TabIndex = 8;
            this.User.Text = "Учетная запись";
            this.User.UseVisualStyleBackColor = false;
            this.User.Click += new System.EventHandler(this.User_Click);
            // 
            // CheckCash
            // 
            this.CheckCash.BackColor = System.Drawing.Color.OrangeRed;
            this.CheckCash.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CheckCash.Location = new System.Drawing.Point(51, 287);
            this.CheckCash.Name = "CheckCash";
            this.CheckCash.Size = new System.Drawing.Size(183, 41);
            this.CheckCash.TabIndex = 10;
            this.CheckCash.Text = "Узнать баланс";
            this.CheckCash.UseVisualStyleBackColor = false;
            this.CheckCash.Click += new System.EventHandler(this.CheckCash_Click);
            // 
            // CheckCashField
            // 
            this.CheckCashField.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CheckCashField.Location = new System.Drawing.Point(51, 219);
            this.CheckCashField.Multiline = true;
            this.CheckCashField.Name = "CheckCashField";
            this.CheckCashField.Size = new System.Drawing.Size(183, 42);
            this.CheckCashField.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(431, 171);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 27);
            this.label1.TabIndex = 12;
            this.label1.Text = "Введите логин";
            // 
            // MinusCash
            // 
            this.MinusCash.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MinusCash.Location = new System.Drawing.Point(436, 228);
            this.MinusCash.Multiline = true;
            this.MinusCash.Name = "MinusCash";
            this.MinusCash.Size = new System.Drawing.Size(181, 42);
            this.MinusCash.TabIndex = 13;
            // 
            // TransferCash
            // 
            this.TransferCash.BackColor = System.Drawing.Color.OrangeRed;
            this.TransferCash.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TransferCash.Location = new System.Drawing.Point(435, 289);
            this.TransferCash.Name = "TransferCash";
            this.TransferCash.Size = new System.Drawing.Size(233, 39);
            this.TransferCash.TabIndex = 14;
            this.TransferCash.Text = "Выполнить перевод";
            this.TransferCash.UseVisualStyleBackColor = false;
            this.TransferCash.Click += new System.EventHandler(this.TransferCash_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(240, 234);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 27);
            this.label2.TabIndex = 15;
            this.label2.Text = "рублей";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(48, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(138, 27);
            this.label3.TabIndex = 16;
            this.label3.Text = "Получатель ";
            // 
            // PlusCash
            // 
            this.PlusCash.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PlusCash.Location = new System.Drawing.Point(53, 58);
            this.PlusCash.Multiline = true;
            this.PlusCash.Name = "PlusCash";
            this.PlusCash.Size = new System.Drawing.Size(181, 43);
            this.PlusCash.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(431, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(178, 27);
            this.label4.TabIndex = 18;
            this.label4.Text = "Сумма перевода";
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox2.Location = new System.Drawing.Point(436, 58);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(173, 43);
            this.textBox2.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(615, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 27);
            this.label5.TabIndex = 20;
            this.label5.Text = "рублей";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(431, 198);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(317, 27);
            this.label6.TabIndex = 21;
            this.label6.Text = "для подтверждения операции";
            // 
            // FormPayment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Red;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.PlusCash);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TransferCash);
            this.Controls.Add(this.MinusCash);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CheckCashField);
            this.Controls.Add(this.CheckCash);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.User);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormPayment";
            this.Text = "FormPayment";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormPayment_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button User;
        private System.Windows.Forms.Button CheckCash;
        private System.Windows.Forms.TextBox CheckCashField;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox MinusCash;
        private System.Windows.Forms.Button TransferCash;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox PlusCash;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}
