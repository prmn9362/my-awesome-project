using System;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class LoginForm : Form
    {
        static TcpClient Client = new TcpClient();
        private const int port = 8888;
        private const string server = "127.0.0.1";
        static NetworkStream stream;
        public LoginForm(ref TcpClient client)
        {
            InitializeComponent();
            Client = client;
        }
        private void ServerConnect_Click(object sender, EventArgs e)
        {
            try
            {
                Client.Connect(server, port);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void RegisterButton_Click(object sender, EventArgs e)
        {
            Hide();
            new RegisterForm(ref Client).Show();
        }
        private void EnterButton_Click(object sender, EventArgs e)
        {
            if ((TextBoxLogin.Text == "") || (TextBoxPassword.Text == ""))
            {
                MessageBox.Show("Проверьте введённые логин и пароль!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    string pass = TextBoxPassword.Text;
                    stream = Client.GetStream();
                    string loginmessage = $"SELECT Password FROM users WHERE Login = '{TextBoxLogin.Text}'";
                    byte[] command = Encoding.UTF8.GetBytes(loginmessage);
                    stream.Write(command, 0, command.Length);
                    stream.Flush();


                    byte[] vs = new byte[64]; // буфер для получаемых данных
                    StringBuilder builder = new StringBuilder();
                    int bait = 0;
                    do
                    {
                        bait = stream.Read(vs, 0, vs.Length);
                        builder.Append(Encoding.UTF8.GetString(vs, 0, bait));
                    }
                    while (stream.DataAvailable);

                    string msag = builder.ToString();
                    if (msag == pass)
                    {
                        MessageBox.Show("Вход выполнен успешно!");
                        Hide();
                        new FormUser(ref Client).Show();
                    }
                    else
                    {
                        MessageBox.Show("Неверный логин или пароль!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
        }
        private void LoginForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}

