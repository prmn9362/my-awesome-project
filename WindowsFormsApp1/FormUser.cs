﻿using System;
using System.Data.SQLite;
using System.Drawing;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class FormUser : Form
    {
        TcpClient Client;
        static NetworkStream stream;
        public FormUser(ref TcpClient client)
        {
            InitializeComponent();
            Client = client;
        }
        private void Upload_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    pictureBox1.ImageLocation = openFileDialog1.FileName;
                }
                else
                {
                    MessageBox.Show("Загрузить картинку не удалось!!!!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void WatchHistory_Click(object sender, EventArgs e)
        {
            Hide();
            new FormHistory(ref Client).Show();
        }
        private void MakePayment_Click(object sender, EventArgs e)
        {
            Hide();
            new FormPayment(ref Client).Show();
        }
        private void LoadSave_Click(object sender, EventArgs e)
        {
            if ((NameField.Text != "") || (SurnameField.Text != "") || (pictureBox1.Image != null))
            {
                NameField.Clear();
                SurnameField.Clear();
                pictureBox1.Image = null;
            }
            stream = Client.GetStream();
            string Message1 = $"SELECT Name FROM users WHERE Login = '{LoginField.Text}'";
            string Message2 = $"SELECT Surname FROM users WHERE Login = '{LoginField.Text}'";
            string Message3 = $"SELECT Image FROM users WHERE Login = '{LoginField.Text}'";
            //Отправка и получении из базы имени
            byte[] command1 = Encoding.UTF8.GetBytes(Message1);
            stream.Write(command1, 0, command1.Length);

            byte[] GetData1 = new byte[128];
            StringBuilder builder1 = new StringBuilder();
            do
            {
                int byte1 = stream.Read(GetData1, 0, GetData1.Length);
                builder1.Append(Encoding.UTF8.GetString(GetData1, 0, byte1));
            }
            while (stream.DataAvailable);
            NameField.Text = builder1.ToString();
            stream.Flush();
            //Отправка и получении из базы фамилии
            byte[] command2 = Encoding.UTF8.GetBytes(Message2);
            stream.Write(command2, 0, command2.Length);
            //Получение и записы из базы имени
            byte[] GetData2 = new byte[128];
            StringBuilder builder2 = new StringBuilder();
            do
            {
                int byte2 = stream.Read(GetData2, 0, GetData2.Length);
                builder2.Append(Encoding.UTF8.GetString(GetData2, 0, byte2));
            }
            while (stream.DataAvailable);
            SurnameField.Text = builder2.ToString();
            stream.Flush();
            //Отправка и получении из базы фотографии
            byte[] command3 = Encoding.UTF8.GetBytes(Message3);
            stream.Write(command3, 0, command3.Length);

            byte[] GetData3 = new byte[10000];
            StringBuilder builder3 = new StringBuilder();
            do
            {
                int byte3 = stream.Read(GetData3, 0, GetData3.Length);
                builder3.Append(Encoding.UTF8.GetString(GetData3, 0, byte3));
            }
            while (stream.DataAvailable);
            MemoryStream ms = new MemoryStream(GetData3, 0, GetData3.Length);
            ms.Write(GetData3, 0, GetData3.Length); 
            pictureBox1.Image = Image.FromStream(ms, true);
        }
        public byte[] imageToByteArray(Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            return ms.ToArray();
        }
        private void Save_Click(object sender, EventArgs e)
        {
            if ((NameField.Text == "") && (SurnameField.Text == "") && (pictureBox1.Image == null) && (LoginField.Text == ""))
            {
                MessageBox.Show("И что мне надо сохранять, если везде пусто?!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    stream = Client.GetStream();
                    if (pictureBox1.Image == null)
                    {
                        string img = null;
                        string Message = $"UPDATE users SET Name = '{NameField.Text}', Surname = '{SurnameField.Text}', Image = '{img}' WHERE Login = '{LoginField.Text}'";
                        byte[] command = Encoding.UTF8.GetBytes(Message);
                        stream.Write(command, 0, command.Length);
                        stream.Flush();
                    }
                    else
                    {
                        byte[] bData = imageToByteArray(pictureBox1.Image);
                        string img = Convert.ToBase64String(bData);
                        string Message = $"UPDATE users SET Name = '{NameField.Text}', Surname = '{SurnameField.Text}', Image = '{img}' WHERE Login = '{LoginField.Text}'";
                        byte[] command = Encoding.UTF8.GetBytes(Message);
                        stream.Write(command, 0, command.Length);
                        stream.Flush();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void FormUser_FormClosing(object sender, FormClosingEventArgs e)
        {
            Hide();
            new LoginForm(ref Client).Show();
        }
    }
}









