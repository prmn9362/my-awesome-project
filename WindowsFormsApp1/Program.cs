﻿using System;
using System.Net.Sockets;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            TcpClient Client = new TcpClient();
            Application.Run(new LoginForm(ref Client));
        }
    }
}
